var Nightmare = require('nightmare');
require('nightmare-evaluate-with-callback')(Nightmare);

function text(){
  console.log("Here");
}

var nightmare = Nightmare({ show: true, openDevTools:{mode: 'detach'}, show: true });

nightmare
  .goto('https://google.com')
  .evaluateWithCallback(function (callback) {
    setTimeout(function() {
      callback(text());
    }, 1000);
  })
  .end()
  .then(function (result) {
    console.log(result)
  })
  .catch(function (error) {
    console.error('Failed:', error);
  });