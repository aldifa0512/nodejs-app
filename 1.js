const Nightmare = require('nightmare')
const nightmare = Nightmare({ show: true, openDevTools:{mode: 'detach'}, show: true,executionTimeout: 100000 });
require('nightmare-evaluate-with-callback')(Nightmare);

nightmare
  .goto('https://duckduckgo.com')
  .type('#search_form_input_homepage', 'github nightmare')
  .click('#search_button_homepage')
  .wait('#r1-0 a.result__a')
  .evaluateWithCallback(function () {
	setTimeout(function() {
      for(var i = 1; i < 10000; i++){
      	console.log(i);
      }
      callback(document.location.href);
      return "Hai"
    }, 0);
  })
  .end()
  .then(function (result) {
    console.log(result)
    console.log('result')
  })
  .catch(function (error) {
    console.error('Failed:', error);
  });