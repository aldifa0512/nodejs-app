$(function(){
	$('#tt').tabs({
		onLoad:function(panel){
			var plugin = panel.panel('options').title;
			panel.find('textarea[name="code-'+plugin+'"]').each(function(){
				var data = $(this).val();
				data = data.replace(/(\r\n|\r|\n)/g, '\n');
				if (data.indexOf('\t') == 0){
					data = data.replace(/^\t/, '');
					data = data.replace(/\n\t/g, '\n');
				}
				data = data.replace(/\t/g, '    ');
				var pre = $('<pre name="code" class="prettyprint linenums"></pre>').insertAfter(this);
				pre.text(data);
				$(this).remove();
			});
		}
	});
	var sw = $(window).width();
	if (sw < 767){
		$('body').layout('collapse', 'west');
	}
	$('.navigation-toggle span').bind('click', function(){
		$('#head-menu').toggle();
	});
});

function openTab(plugin){
	if ($('#tt').tabs('exists',plugin)){
		$('#tt').tabs('select', plugin);
	} else {
    //Merubah title tab menjadi applicable url
    var pluginReplaced = plugin.replace(/\s/g, '');
    var href = pluginReplaced.toLowerCase();
		$('#tt').tabs('add',{
			title:plugin,
			href:href,
			closable:true,
			bodyCls:'content-doc',
			extractor:function(data){
				return data;
			}
		});
	}
}
