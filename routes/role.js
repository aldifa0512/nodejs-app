var express = require('express');
var router = express.Router();
var mongo = require('mongodb').MongoClient;
var objectId = require('mongodb').ObjectID;
var assert = require('assert');
var fs = require('fs');

var url = 'mongodb://localhost:27017/app';
var Role = require('../models/role');

// Cek otentikasi user
function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	} else {
		req.flash('error_msg','<div class="alert alert-danger">You are not logged in</div>');
		res.redirect('/login');
	}
}

// Kalau dibutuhkan----------------------------------------
function protectAdminRoutes(req, res, next){
	if(req.user.access_level != 1){
		console.log("Sorry! only for administartor!");
	}else{
		return next();
	}
}

// Get page content for Role Setting Page Page
router.get('/', ensureAuthenticated, protectAdminRoutes, function(req, res){
	res.render('pages/role/main-content');
});

// Get data untuk halaman setting role
router.post('/get-roles', ensureAuthenticated, protectAdminRoutes, function(req, res){
	var result = [];
	mongo.connect(url, function(err, db){
		assert.equal(null, err);
		var cursor = db.collection('roles').find();
		cursor.forEach(function(doc, err){
			assert.equal(null, err);
			result.push(doc);
		}, function(){
			db.close();
			res.send(result)
		});
	});
});

// Add New role
router.post('/add-role', protectAdminRoutes, function(req, res) {

    var role_name = req.body.role_name;
    var role_code = req.body.role_code;
	var description = req.body.description;
	var status = req.body.status;

	// Validation
	req.checkBody('role_name', 'Role Name is required').notEmpty();
	req.checkBody('role_code', 'Role Code is required').notEmpty();
	req.checkBody('description', 'Description is required').notEmpty();
	req.checkBody('status', 'Status is required').notEmpty();

	var errors = req.validationErrors();

	var data_role =
    {
		role_name: role_name,
		role_code: role_code,
		description: description,
		status: status,
	};

	if(errors){
		// Disini Kirim Validation Error Json untuk JEasy UI nya
		console.log("Data tidak valid :" + errors)
	} else {
		var newRole = new Role(data_role);

		Role.createRole(newRole, function(err, role){
			if(err) throw err;
			console.log(role);
			res.send(role);
		});
	}
});

// Update Role
router.post('/update-role', ensureAuthenticated, protectAdminRoutes, function(req, res){
	var id = req.query.id;

	var role_name = req.body.role_name;
    var role_code = req.body.role_code;
	var description = req.body.description;
	var status = req.body.status;

	// Validation
	req.checkBody('role_name', 'Role Name is required').notEmpty();
	req.checkBody('role_code', 'Role Code is required').notEmpty();
	req.checkBody('description', 'Description is required').notEmpty();
	req.checkBody('status', 'Status is required').notEmpty();

	var errors = req.validationErrors();

	var data_role =
    {
		role_name: role_name,
		role_code: role_code,
		description: description,
		status: status,
	};
	
	if(errors){
		// Disini Kirim Validation Error Json untuk JEasy UI nya
		console.log("Data tidak valid :" + errors)
	} else {
		Role.updateRole(id, data_role, function(err, role){
			if(err) throw err;
			console.log(role);
			res.send(role);
		});
	}
});

// Delete Role
router.post('/delete-role', ensureAuthenticated, protectAdminRoutes, function(req, res){
	var id = req.body.id;
	Role.deleteRole(id, function(err, role){
		if(err) throw err;
		res.send({success: true});
	});
});

module.exports = router;
