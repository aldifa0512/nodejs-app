var express = require('express');
var router = express.Router();
var mongo = require('mongodb').MongoClient;
var objectId = require('mongodb').ObjectID;
var assert = require('assert');
var fs = require('fs');

var url = 'mongodb://localhost:27017/app';
var User = require('../models/user');

// Cek otentikasi user
function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	} else {
		req.flash('error_msg','<div class="alert alert-danger">You are not logged in</div>');
		res.redirect('/login');
	}
}

// Kalau dibutuhkan----------------------------------------
function protectAdminRoutes(req, res, next){
	if(req.user.access_level != 1){
		console.log("Sorry! only for administartor!");
	}else{
		return next();
	}
}

// Get page content for User Management Page
router.get('/', ensureAuthenticated, protectAdminRoutes, function(req, res){
	var result = [];
	mongo.connect(url, function(err, db){
		assert.equal(null, err);
		var cursor = db.collection('roles').find();
		cursor.forEach(function(doc, err){
			assert.equal(null, err);
			result.push(doc);
		}, function(){
			db.close();
			res.render('pages/user-management/main-content', {role: result});
		});
	});
});

// Get data for User Management Page
router.post('/get-users', ensureAuthenticated, protectAdminRoutes, function(req, res){
	var result = [];
	mongo.connect(url, function(err, db){
		assert.equal(null, err);
		var cursor = db.collection('users').find();
		cursor.forEach(function(doc, err){
			assert.equal(null, err);
			result.push(doc);
		}, function(){
			db.close();
			res.send(result)
		});
	});
});

// Add New user
router.post('/add-user', protectAdminRoutes, function(req, res) {

    var username = req.body.username.replace(/\s/g, '');
	var password = req.body.password.replace(/\s/g, '');
	var fullname = req.body.fullname;
	var telephone = req.body.telephone;
	var email = req.body.email;
	var access_level = req.body.access_level;
	var language = req.body.language;
	var user_POS = req.body.user_POS;
	var printing_profile = req.body.printing_profile;
	var use_popup = req.body.use_popup

	// Validation
	req.checkBody('fullname', 'Name is required').notEmpty();
	req.checkBody('email', 'Email is required').notEmpty();
	req.checkBody('username', 'Username is required').notEmpty();
	req.checkBody('password', 'Password is required').notEmpty();
	req.checkBody('telephone', 'No. Telephone is required').notEmpty();
	req.checkBody('access_level', 'Access Level is required').notEmpty();
	req.checkBody('language', 'Language is required').notEmpty();
	req.checkBody('user_POS', 'User\'s POS is required').notEmpty();
	req.checkBody('printing_profile', 'Printing Profile is required').notEmpty();

	var errors = req.validationErrors();

	var data_user =
    {
		username: username,
		password: password,
		fullname: fullname,
		telephone: telephone,
		email: email,
		access_level: access_level,
		language: language,
		user_POS: user_POS,
		printing_profile: printing_profile,
		use_popup: use_popup
	};

	if(errors){
		console.log(errors);
		res.render('pages/register',{
			errors:errors
		});
	} else {
		var newUser = new User(data_user);

		User.createUser(newUser, function(err, user){
			if(err) throw err;
			console.log(user);
			res.send(user);
		});
	}
});

// Update User
router.post('/update-user', ensureAuthenticated, protectAdminRoutes, function(req, res){
	var id = req.query.id;

	var username = req.body.username.replace(/\s/g, '');
	var password = req.body.password.replace(/\s/g, '');
	var fullname = req.body.fullname;
	var telephone = req.body.telephone;
	var email = req.body.email;
	var access_level = req.body.access_level;
	var language = req.body.language;
	var user_POS = req.body.user_POS;
	var printing_profile = req.body.printing_profile;
	var use_popup = req.body.use_popup

	// Validation
	req.checkBody('fullname', 'Name is required').notEmpty();
	req.checkBody('email', 'Email is required').notEmpty();
	req.checkBody('username', 'Username is required').notEmpty();
	req.checkBody('password', 'Password is required').notEmpty();
	req.checkBody('telephone', 'No. Telephone is required').notEmpty();
	req.checkBody('access_level', 'Access Level is required').notEmpty();
	req.checkBody('language', 'Language is required').notEmpty();
	req.checkBody('user_POS', 'User\'s POS is required').notEmpty();
	req.checkBody('printing_profile', 'Printing Profile is required').notEmpty();

	var errors = req.validationErrors();

	var data_user =
    {
		username: username,
		password: password,
		fullname: fullname,
		telephone: telephone,
		email: email,
		access_level: access_level,
		language: language,
		user_POS: user_POS,
		printing_profile: printing_profile,
		use_popup: use_popup
	};

	if(errors){
		console.log(errors);
		res.render('pages/register',{
			errors:errors
		});
	} else {

		User.updateUser(id, data_user, function(err, user){
			if(err) throw err;
			console.log(user);
			res.send(user);
		});
	}
});

// Delete User
router.post('/delete-user', ensureAuthenticated, protectAdminRoutes, function(req, res){
	var id = req.body.id;
	User.deleteUser(id, function(err, user){
		if(err) throw err;
		res.send({success: true});
	});
});

module.exports = router;
