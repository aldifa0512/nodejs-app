var express = require('express');
var router = express.Router();
var mongo = require('mongodb').MongoClient;
var objectId = require('mongodb').ObjectID;
var assert = require('assert');
var fs = require('fs');

var url = 'mongodb://localhost:27017/app';
var ESubscription = require('../models/ESubscription');

// Cek otentikasi user
function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	} else {
		req.flash('error_msg','<div class="alert alert-danger">You are not logged in</div>');
		res.redirect('/login');
	}
}

function protectOptRoutes(req, res, next){
	if(req.user.access_level != 2){
		console.log("Sorry! OPT user only");
	}else{
		return next();
	}
}

// Get page content for Email Subscription
router.get('/', ensureAuthenticated, protectOptRoutes, function(req, res){
	res.render('pages/esubscription/main-content');
});

// Running Rekon Email Subscription Bot
router.get('/run', ensureAuthenticated, protectOptRoutes, function(req, res){
	ESubscription.run();
});

module.exports = router;