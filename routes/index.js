var express = require('express');
var router = express.Router();
var mongo = require('mongodb').MongoClient;
var objectId = require('mongodb').ObjectID;
var assert = require('assert');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var User = require('../models/user');

router.get('/', function(req, res, next) {
  res.redirect('/login');
});

var validateRequest = function(req, callback) {
	req.checkBody('username', 'Username is required').notEmpty();
	req.checkBody('password', 'Password is required').notEmpty();
	// req.checkBody('recaptcha', 'Recaptcha is required').notEmpty();

	var errors = req.validationErrors();
	console.log(errors);
	if(errors){
		callback(errors);		
	}else{
		callback(null);
	}
}
// --------------------------------------------------------------
// Kodingan Login Aplikasi---------------------------------------

// Login
router.get('/login', function(req, res){
	var user =  req.user;
	res.render('pages/login', {title: 'Login'});
});

// Register User
router.get('/register', function(req, res){
	var fullname = 'admin';
	var email = 'admin@mail.com';
	var username = 'admin';
	var access_level = '1';
	var password = 'admin123';
	var password2 = 'admin123';

	// Validation
	// req.checkBody('fullname', 'Name is required').notEmpty();
	// req.checkBody('email', 'Email is required').notEmpty();
	// req.checkBody('email', 'Email is not valid').isEmail();
	// req.checkBody('username', 'Username is required').notEmpty();
	// req.checkBody('password', 'Password is required').notEmpty();
	// req.checkBody('password2', 'Passwords do not match').equals(req.body.password);

	var errors = req.validationErrors();

	if(errors){
		console.log(errors);
		res.render('pages/register',{
			errors:errors
		});
	} else {
		var newUser = new User({
			fullname: fullname,
			email:email,
			username: username,
			password: password,
			access_level: access_level
		});

		User.createUser(newUser, function(err, user){
			if(err) throw err;
			console.log(user);
		});

		req.flash('success_msg', '<div class="alert alert-success">You are registered and can now login</div>');

		res.redirect('/login');
	}
});

passport.use(new LocalStrategy(
  function(username, password, done) {
   User.getUserByUsername(username, function(err, user){
   	if(err) throw err;
   	if(!user){
   		return done(null, false, {message: '<div class="alert alert-danger">Username is not registred</div>'});
   	}

   	console.log("Typed in password from client: " + password);
   	console.log("Saved hashed password from db: " + user.password);
   	User.comparePassword(password, user.password, function(err, isMatch){
   		if(err) throw err;
   		if(isMatch){
   			return done(null, user);
   		} else {
   			return done(null, false, {message: '<div class="alert alert-danger">Invalid password</div>'});
   		}
   	});
  });
}));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.getUserById(id, function(err, user) {
    done(err, user);
  });
});

router.post('/login', function (req, res, next) { 
    validateRequest(req, function (errors) {
        if (!errors) {
        	return next();
        } else {
            res.render('pages/login',{
				errors:errors, title: "Login"
			});
        }
    });
},
passport.authenticate('local', {
	successRedirect:'/dashboard', 
	failureRedirect:'/login',
	failureFlash: true,
	successFlash: 'This is flash data from server' 
}));

router.get('/logout', function(req, res){
	req.logout();

	req.flash('success_msg', '<div class="alert alert-success">You are logged out</div>');

	res.redirect('/login');
});

router.get('/bot', function(req, res){
	const { exec } = require('child_process');
	exec('node bca.js', (err, stdout, stderr) => {
	  if (err) {
	      return;
	    }
	  console.log(`stdout: ${stdout}`);
	  console.log(`stderr: ${stderr}`);
	});
});
// Kodingan Login Aplikasi---------------------------------------
// --------------------------------------------------------------

module.exports = router;
