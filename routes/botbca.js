var express = require('express');
var router = express.Router();
var mongo = require('mongodb').MongoClient;
var objectId = require('mongodb').ObjectID;
var assert = require('assert');
var fs = require('fs');

var url = 'mongodb://localhost:27017/app';
var bca = require('../models/bca');

// Cek otentikasi user
function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	} else {
		req.flash('error_msg','<div class="alert alert-danger">You are not logged in</div>');
		res.redirect('/login');
	}
}

// Kalau dibutuhkan----------------------------------------
function protectAdminRoutes(req, res, next){
	if(req.user.access_level != 1){
		console.log("Sorry! only for administartor!");
	}else{
		return next();
	}
}

// Get page content for BCA Bot
router.get('/', ensureAuthenticated, protectAdminRoutes, function(req, res){
	res.render('pages/bot-bca/main-content');
});

// Get data for Bot Page
router.post('/get-account', ensureAuthenticated, protectAdminRoutes, function(req, res){
	var result = [];
	mongo.connect(url, function(err, db){
		assert.equal(null, err);
		var cursor = db.collection('bots').find();
		cursor.forEach(function(doc, err){
			assert.equal(null, err);
			result.push(doc);
		}, function(){
			db.close();
			res.send(result)
		});
	});
});

// Running BCA Bot
router.get('/bots', ensureAuthenticated, protectAdminRoutes, function(req, res){
	bca.run();
});

module.exports = router;