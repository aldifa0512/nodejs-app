var express = require('express');
var router = express.Router();
var mongo = require('mongodb').MongoClient;
var objectId = require('mongodb').ObjectID;
var assert = require('assert');
var fs = require('fs');

var url = 'mongodb://localhost:27017/app';
var User = require('../models/user');
var Menu = require('../models/menu');

// Get All Menu
function getMenus(url){
	fs.readFile('ftp/menu.json', function(err, data) {
    if(err){
    	console.log("Failed Reading ftp/accountStatement/product.json");
    }else{
    	var menus = JSON.parse(data);
    	var result = [];
		mongo.connect(url, function(err, db){
			assert.equal(null, err);
			var cursor = db.collection('menus').find();
			cursor.forEach(function(doc, err){
				assert.equal(null, err);
				result.push(doc);
			}, function(err){
				if(err) console.log(err);
				db.close();
				return result;
			});
		});
    }	
}

// Cek otentikasi user
function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	} else {
		req.flash('error_msg','<div class="alert alert-danger">You are not logged in</div>');
		res.redirect('/login');
	}
}

// Kalau dibutuhkan----------------------------------------
function protectAdminRoutes(req, res, next){
	if(req.user.access_level != 2){
		console.log("Sorry! only for administartor!");
	}else{
		return next();
	}
}

// Dashboard page
router.get('/', ensureAuthenticated, function(req, res){
	var result = [];
	var user = req.user;
	mongo.connect(url, function(err, db){
		assert.equal(null, err);
		var cursor = db.collection('users').find();
		cursor.forEach(function(doc, err){
			assert.equal(null, err);
			result.push(doc);
		},function(){
			db.close();
			var menu = [];
			var sendEmail = [];
			mongo.connect(url, function(err, db){
				assert.equal(null, err);
				var cursor = db.collection('menus').find({ menu_role: user.access_level});
				cursor.forEach(function(doc, err){
					assert.equal(null, err);
                    if(
                        doc.menu_name == "PENGIRIMAN LAPORAN CONF SUBSCRIPTION" ||
                        doc.menu_name == "PENGIRIMAN LAPORAN CONF REDEMPTION" 
                    ){
                        sendEmail.push(doc.menu_name);
                    }else{
                    	menu.push(doc);
                    }
				}, function(err){
					if(err) console.log(err);
					db.close();
					res.render('pages/dashboard', {
						title: 'Dashboard Robotik BNI', 
						users: result, 
						menu: menu,
						sendEmail: sendEmail,
						user: user
					})
				});
			});
		});
	});
});

module.exports = router;
