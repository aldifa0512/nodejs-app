var express = require('express');
var router = express.Router();
var mongo = require('mongodb').MongoClient;
var objectId = require('mongodb').ObjectID;
var assert = require('assert');
var request = require('request');
var fs = require('fs');

var url = 'mongodb://localhost:27017/app';
var EAccountStatement = require('../models/testcafe');

// Cek otentikasi user
function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	} else {
		req.flash('error_msg','<div class="alert alert-danger">You are not logged in</div>');
		res.redirect('/login');
	}
}

// Get page content for Rekon RK Dengan Reksadana Bot
router.get('/', ensureAuthenticated, function(req, res){
	res.render('pages/eaccountstatement/main-content');
});

// Get data for Rekon RK Dengan Reksadana Bot Page
router.post('/get-data', ensureAuthenticated, function(req, res){
	var result = [];
	mongo.connect(url, function(err, db){
		assert.equal(null, err);
		var cursor = db.collection('bots').find();
		cursor.forEach(function(doc, err){
			assert.equal(null, err);
			result.push(doc);
		}, function(){
			db.close();
			res.send(result)
		});
	});
});

// Running Pengiriman Email Account Statement
router.get('/run', ensureAuthenticated, function(req, res){
	req.connection.setTimeout( 5184000 ); // ten minutes
	EAccountStatement.run();
});

module.exports = router;
