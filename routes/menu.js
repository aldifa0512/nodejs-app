var express = require('express');
var router = express.Router();
var mongo = require('mongodb').MongoClient;
var objectId = require('mongodb').ObjectID;
var assert = require('assert');
var fs = require('fs');

var url = 'mongodb://localhost:27017/app';
var Menu = require('../models/menu');

// Cek otentikasi user
function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	} else {
		req.flash('error_msg','<div class="alert alert-danger">You are not logged in</div>');
		res.redirect('/login');
	}
}

// Kalau dibutuhkan----------------------------------------
function protectAdminRoutes(req, res, next){
	if(req.user.access_level != 1){
		console.log("Sorry! only for administartor!");
	}else{
		return next();
	}
}

// Get page content for Menu Setting Page Page
router.get('/', ensureAuthenticated, protectAdminRoutes, function(req, res){
	var result = [];
	mongo.connect(url, function(err, db){
		assert.equal(null, err);
		var cursor = db.collection('roles').find();
		cursor.forEach(function(doc, err){
			assert.equal(null, err);
			result.push(doc);
		}, function(){
			db.close();
			res.render('pages/menu/main-content', {role: result});
		});
	});
});

// Get data untuk halaman setting menu
router.post('/get-menus', ensureAuthenticated, protectAdminRoutes, function(req, res){
	var result = [];
	mongo.connect(url, function(err, db){
		assert.equal(null, err);
		var cursor = db.collection('menus').find();
		cursor.forEach(function(doc, err){
			assert.equal(null, err);
			result.push(doc);
		}, function(){
			db.close();
			res.send(result)
		});
	});
});

// Add New menu
router.post('/add-menu', protectAdminRoutes, function(req, res) {

    var menu_name = req.body.menu_name;
    var menu_role = req.body.menu_role;
	var description = req.body.description;
	var status = req.body.status;

	// Validation
	req.checkBody('menu_name', 'Menu Name is required').notEmpty();
	req.checkBody('menu_role', 'Menu Role is required').notEmpty();
	req.checkBody('description', 'Description is required').notEmpty();
	req.checkBody('status', 'Status is required').notEmpty();

	var errors = req.validationErrors();

	var data_menu =
    {
		menu_name: menu_name,
		menu_role: menu_role,
		description: description,
		status: status,
	};

	if(errors){
		// Disini Kirim Validation Error Json untuk JEasy UI nya
		console.log("Data tidak valid :" + errors)
	} else {
		var newMenu = new Menu(data_menu);

		Menu.createMenu(newMenu, function(err, menu){
			if(err) throw err;
			console.log(menu);
			res.send(menu);
		});
	}
});

// Update Menu
router.post('/update-menu', ensureAuthenticated, protectAdminRoutes, function(req, res){
	var id = req.query.id;

	var menu_name = req.body.menu_name;
	var menu_role = req.body.menu_role;
	var description = req.body.description;
	var status = req.body.status;

	// Validation
	req.checkBody('menu_name', 'Menu Name is required').notEmpty();
	req.checkBody('menu_role', 'Menu Role is required').notEmpty();
	req.checkBody('description', 'Description is required').notEmpty();
	req.checkBody('status', 'Status is required').notEmpty();

	var errors = req.validationErrors();

	var data_menu =
    {
		menu_name: menu_name,
		menu_role: menu_role,
		description: description,
		status: status,
	};
	
	if(errors){
		// Disini Kirim Validation Error Json untuk JEasy UI nya
		console.log("Data tidak valid :" + errors)
	} else {
		Menu.updateMenu(id, data_menu, function(err, menu){
			if(err) throw err;
			console.log(menu);
			res.send(menu);
		});
	}
});

// Delete Menu
router.post('/delete-menu', ensureAuthenticated, protectAdminRoutes, function(req, res){
	var id = req.body.id;
	Menu.deleteMenu(id, function(err, menu){
		if(err) throw err;
		res.send({success: true});
	});
});

module.exports = router;
