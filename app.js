var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var flash = require('connect-flash');
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongo = require('mongodb');
var mongoose = require('mongoose');
const url = require('url');

mongoose.connect('mongodb://localhost/app');
var db = mongoose.connection;

var index = require('./routes/index');
var dashboard = require('./routes/dashboard');
var usermanagement = require('./routes/userManagement');
var botbca = require('./routes/botbca');
var role = require('./routes/role');
var menu = require('./routes/menu');
var scrape = require('./routes/scrape');
var EAccountStatement = require('./routes/EAccountStatement');
var ESubscription = require('./routes/ESubscription');
var ERedemption = require('./routes/ERedemption');
var testcafe = require('./routes/testcafe');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Express Session
app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true
}));

// Passport init
app.use(passport.initialize());
app.use(passport.session());

// Express Validator
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));

// Connect Flash
app.use(flash());

// Global Vars
app.use(function (req, res, next) {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.errors = req.flash('errors');
  res.locals.error = req.flash('error');
  res.locals.user = req.user || null;
  next();
});

app.use('/', index);
app.use('/dashboard', dashboard);
app.use('/usermanagement', usermanagement);
app.use('/botbca', botbca);
app.use('/role', role);
app.use('/menu', menu);
app.use('/scrape', scrape);
app.use('/pengirimanaccountstatement', EAccountStatement);
app.use('/pengirimanlaporanconfsubscription', ESubscription);
app.use('/ERedemption', ERedemption);
app.use('/testcafe', testcafe);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('pages/error');
});

module.exports = app;
