module.exports = {
	run: function() {
		require('events').EventEmitter.prototype._maxListeners = 100;

		var Nightmare = require('nightmare');
		var Promise = require('q').Promise;
		var cheerio = require('cheerio');
		var result = [];
		var mongoose = require('mongoose');

		var BotSchema = mongoose.Schema({
			tanggal			: {type: String},
			nama_transaksi	: {type: String},
			debit			: {type: String},
			credit			: {type: String}
		});
		var Bot = mongoose.model('Bot', BotSchema);

		var x = Date.now();
		var nightmare = Nightmare({ show: true });

		Promise.resolve(nightmare
			.goto('https://ibank.klikbca.com/')
			.wait('input[type="Submit"]')
			.type('input[id="user_id"]', 'waldisya1020')
			.type('input[id="pswd"]', '120462')
			.click('input[type="Submit"]')
			.wait(5000)
			.evaluate(function() {
				document.getElementsByName("menu")[0].contentDocument.querySelectorAll("a[href*='acc']")[0].click();
			})
			.wait(3000)
			.evaluate(function() {
				document.getElementsByName("menu")[0].contentDocument.querySelectorAll("[onclick*='acc']")[0].click();
			})
			.wait(2000)
			.evaluate(function() {
				document.getElementsByName("atm")[0].contentDocument.querySelectorAll('input[type="radio"][value="2"]')[0].click();
			})
			.wait(2000)
			.evaluate(function() {
				document.getElementsByName("atm")[0].contentDocument.querySelectorAll('input[type="submit"][value*="View Account Statement"]')[0].click();
			})
			.wait(5000)
			.evaluate(function() {
				var body = document.getElementsByName("atm")[0].contentDocument.body.innerHTML;
				return body;
			})
		).then(function(html) {
			var $ = cheerio.load(html);

			$('tr').each(function (i, el) {
				var data = {};
				var $tds = $(this).find('td');
				var str = $tds.eq(0).text();
				str = String(str).replace(/^\s+|\s+$/g, '');
				str = String(str).replace(/^\n+|\r+$/g, '');
				var pattern =/^([0-9]{2})\/([0-9]{2})$/;
				if(pattern.test(str))
				{
					data.date = String($tds.eq(0).text()).replace(/^\n+|\r+$/g, '');
					data.name = String($tds.eq(1).text()).replace(/^\n+|\r+$/g, '');

					if(String($tds.eq(4).text()).replace(/^\n+|\r+$/g, '')=="DB")
					{
						data.debit = String($tds.eq(3).text()).replace(/^\n+|\r+$/g, '');
						data.credit = '0.00';
					}
					else
					{
						data.debit = '0.00';
						data.credit = String($tds.eq(3).text()).replace(/^\n+|\r+$/g, '');	
					}
					result.push(data);			
				}
			});
			for (var s in result) {
				var data_record = {'tanggal': result[s].date, 'nama_transaksi': result[s].name, 'debit': result[s].debit, 'credit': result[s].credit}; 
				console.log(data_record);
				var newRecord = new Bot(data_record);
			    newRecord.save(function (err, fluffy) {
				    if (err) return console.error(err);
			  	});
			}
			console.log("done in " + (Date.now()-x) + "ms");
			data = { "success": true, "data": result };
			return data;
		}).then(function(result) {
			// console.log(result);
			document.getElementsByName("header")[0].contentDocument.querySelectorAll("[onclick*='logout']")[0].click();
			return nightmare.end();
		}, function(err) {
			console.error(err); 	
			nightmare.end();	
		});
	},
};
