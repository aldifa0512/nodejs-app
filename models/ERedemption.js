module.exports = {
	run: function() {
		require('events').EventEmitter.prototype._maxListeners = 100;

		var Nightmare = require('nightmare');
		require('nightmare-evaluate-with-callback')(Nightmare);
		var Promise = require('q').Promise;
		var cheerio = require('cheerio');
		var mongoose = require('mongoose');
		var request = require('request');
		var fs = require('fs');

		var x = Date.now();
		var nightmare = Nightmare({ show: true });

		function scrapeAccountStatementPage(body){
			var $ = cheerio.load(body);
			var product = {};
			var agent = {};

			// Scrape Product List
			var e = 0;
			$('#ContentPlaceHolder1_cbxProduct_DDD_L_LBT .dxeListBoxItemRow .dxeListBoxItem').each(function(i){
				console.log($(this));
				var productValue = $(this).html().split(" ")[0];
				productValue = productValue.substr(1).slice(0, -1);
				product[e] = {key: productValue, name: $(this).html()};
				e++;
			});

			fs.writeFile('ftp/redemption/product.json', JSON.stringify(product, null, 4), function(err){
			 console.log('Written!, ftp/redemption/product.json file');
			})
		}

		function startLoopEmail(){
			fs.readFile('ftp/redemption/product.json', function(err, data) {
			    if(err){
			    	console.log("Failed Reading ftp/redemption/product.json");
			    }else{
			    	var date = new Date('2019/03/29').getTime();
			    	var count = 0;
			    	var products = JSON.parse(data);
			    	var totalProducts = Object.keys(products).length;
					var forceCallback = function(products) {
					    loopProducts(products[count], function(){
				    		count++;

					        if(count < totalProducts) {
					            forceCallback(products);   
					        }
					    }); 
					}
					var loopProducts = function(p, callback) {
					    var product = p.key;
					    var productName = p.name;
					    console.log("Sending E-mail for Product " + productName + " " + (count + 1) + " from " + totalProducts);

						Promise.resolve(nightmare
							.goto('http://192.168.3.52/eMail/Pages/SIAR/LaporanAkun.aspx?MenuId=862&ssoid=QYVIBXXOEFO7T0I94VUG73FCZIMXCUOY7SMCVG_5ZA7YF')
							.wait('#ContentPlaceHolder1_cbxProduct_VI')
							.evaluate(function(product) {
								document.querySelector("#ContentPlaceHolder1_cbxProduct_VI").value=product;
							}, product)
							.wait('#ContentPlaceHolder1_dteDate_Raw')
							.evaluate(function(date) {
								document.querySelector("#ContentPlaceHolder1_dteDate_Raw").value=date;
							}, date)
							.wait('#ContentPlaceHolder1_cbxAgent_VI')
							.evaluate(function() {
								document.querySelector("#ContentPlaceHolder1_cbxAgent_VI").value="%";
							})
							.wait('#ContentPlaceHolder1_btnFind_CD')
							.evaluate(function() {
								document.querySelector("#ContentPlaceHolder1_btnFind_CD").click()
							})
							.wait(".dxpSummary")
							.evaluateWithCallback(function (callback) {
								// Get summary result
								var pageSummary = document.getElementsByClassName("dxpSummary")[0].innerHTML.split(" ");
							   	var totalPages = 5;//parseInt(pageSummary[3]);
								var totalResults = parseInt(pageSummary[4].substr(1));
								var count = 0;
								var page = 0;
								var nextProduct = false;
								var timeout = totalPages * 11500;
								function loopPages(count, page){
									setTimeout(function(){
										var loopThisPageNo = (count + 10);
										for (i = count; i < loopThisPageNo; i++) { 
											document.getElementById("ContentPlaceHolder1_ASPxGridView1_DXSelBtn" + i + "_D").className="dxICheckBox  dxWeb_edtCheckBoxChecked";
											console.log("Checking page: " + (page + 1) + " row: " + (i + 1));
										}

										if(totalPages > page){
											page++;
											setTimeout(function(){
												aspxGVPagerOnClick('ContentPlaceHolder1_ASPxGridView1','PN' + page);
												console.log('Jump to page number: ' + (page+1))
												loopPages(i, page);								
											}, 500);
										}else{
											nextProduct = true;
										}
									}, 11000);
								}
								loopPages(count, page);
								setTimeout(function() {
									if(nextProduct){
										return nextProduct;
									}
								}, timeout);
							})
						)
						.then(function(nextProduct) {
							console.log(nextProduct);
							if(nextProduct){
								callback(nextProduct);
							}
							return nightmare.end();
						}, function(err) {
							console.error(err); 	
							// nightmare.end();	
						});
					}
					forceCallback(products);
			    }
		    });
		}

		var SECUS = 'http://192.168.3.52/eMail/Pages/SIAR/ConfUnitRed.aspx?MenuId=899&ssoid=SMP8RN2346K1KN4CTCUI8QT7UGQOB2V5BNA0D4SY4M00X';
		request(SECUS, function(error, response, body){
			if(!error){
				scrapeAccountStatementPage(body);
				startLoopEmail()
			}else{
				console.log("Failed to load http://192.168.3.52/eMail/Pages/SIAR/ConfUnitRed.aspx");
				console.log(error);
			}
		});
	}
};

