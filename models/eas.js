module.exports = {
	run: function() {
		// setInterval(function(){
			require('events').EventEmitter.prototype._maxListeners = 100;
			var Nightmare = require('nightmare');
			Nightmare.action('waitforunload', function (done) {
			    this.evaluate_now(function() {
			        return new Promise(function(resolve, reject) {
			            window.onbeforeunload = function() {
			                resolve();
			            };
			        });
			    }, done)
			});
			require('nightmare-evaluate-with-callback')(Nightmare);
			var Promise = require('q').Promise;
			var cheerio = require('cheerio');
			var mongoose = require('mongoose');
			var request = require('request');
			var fs = require('fs');
			var nightmare = Nightmare({ show: true, openDevTools:{mode: 'detach'}, show: true,executionTimeout: 100000 });

			var SEASM = 'http://192.168.3.52/eMail/Pages/SIAR/LaporanAkun.aspx?MenuId=862&ssoid=QYVIBXXOEFO7T0I94VUG73FCZIMXCUOY7SMCVG_5ZA7YF';
			request(SEASM, function(error, response, body){
				if(!error){
					var $ = cheerio.load(body);
					var product = {};
					var agent = {};

					// Scrape Product List
					var e = 0;
					$('#ContentPlaceHolder1_cbxProduct_DDD_L_D .dxeListBoxItemRow .dxeListBoxItem').each(function(i){
						var productValue = $(this).html().split(" ")[0];
						productValue = productValue.substr(1).slice(0, -1);
						product[e] = {key: productValue, name: $(this).html()};
						e++;
					});

					// Scrape Agent List
					var f = 0;
					$('#ContentPlaceHolder1_cbxAgent_DDD_L_D .dxeListBoxItemRow .dxeListBoxItem').each(function(i){
						agent[f] = {name: $(this).html()};
						f++;
					});

					fs.writeFile('ftp/accountStatement/product.json', JSON.stringify(product, null, 4), function(err){
					 console.log('Written!, ftp/accountStatement/product.json file');
					})
					fs.writeFile('ftp/accountStatement/agent.json', JSON.stringify(agent, null, 4), function(err){
					 console.log('Written!, ftp/accountStatement/agent.json file');
					})

					// New Loop Products Function 
			    	var pageNo;
			    	var product;
					var products;
					var productKey;
					var totalPages;
					var productName;
					var nextProduct;
					var totalProducts;
					var countProduct = 1; 
					var date = new Date('2019/04/01').getTime();
					fs.readFile('ftp/accountStatement/product.json', function(err, data) {
					    if(err){
					    	console.log("Failed Reading ftp/accountStatement/product.json");
					    }else{
							var countProduct = 1; 
					    	var products = JSON.parse(data);
					    	var product = products[countProduct];
					    	var nextProduct = products[countProduct + 1];
					    	var totalProducts = Object.keys(products).length;
					    	var productKey = product.key;
						    var productName = product.name;
							
					    	// Save next product data to file, which will be called later
					    	fs.writeFile('ftp/accountStatement/nextProduct.json', JSON.stringify(nextProduct, null, 4), function(err){
								console.log('Written!, ftp/accountStatement/nextProduct.json file');
							})
					    	
						    Promise.resolve(nightmare
								.goto('http://192.168.3.52/eMail/Pages/SIAR/LaporanAkun.aspx?MenuId=862&ssoid=QYVIBXXOEFO7T0I94VUG73FCZIMXCUOY7SMCVG_5ZA7YF')
								.wait('#ContentPlaceHolder1_cbxProduct_VI')
								.evaluate(function(product) {
									document.querySelector("#ContentPlaceHolder1_cbxProduct_VI").value=productKey;
								}, product)
								.wait('#ContentPlaceHolder1_dteDate_Raw')
								.evaluate(function(date) {
									document.querySelector("#ContentPlaceHolder1_dteDate_Raw").value=date;
								}, date)
								.wait('#ContentPlaceHolder1_cbxAgent_VI')
								.evaluate(function() {
									document.querySelector("#ContentPlaceHolder1_cbxAgent_VI").value="%";
								})
								.wait('#ContentPlaceHolder1_btnFind_CD')
								.evaluate(function() {
									document.querySelector("#ContentPlaceHolder1_btnFind_CD").click()
								})
								.wait(".dxpSummary")
								.evaluateWithCallback(function (products, i) {
									// Get summary result
									var pageSummary = document.getElementsByClassName("dxpSummary")[0].innerHTML.split(" ");
								   	var totalPages = parseInt(pageSummary[3]);
									var totalResults = parseInt(pageSummary[4].substr(1));
									var count = 0;
									var page = 0;
									var counter = 1;
									var nextProduct = false;
									var timeout = totalPages * 11500;
									
									function loopPages(count, page){
										setTimeout(function(){
											var loopThisPageNo = (count + 10);
											for (i = count; i < loopThisPageNo; i++) { 
												document.getElementById("ContentPlaceHolder1_ASPxGridView1_DXSelBtn" + i + "_D").click();
												console.log("Checking page: " + (page + 1) + " row: " + (i + 1));
											}

											if(totalPages > page){
												page++;
												document.getElementById("ContentPlaceHolder1_btnSendEmail_CD").click();
												setTimeout(function(){
													// catchLog()
													aspxGVPagerOnClick('ContentPlaceHolder1_ASPxGridView1','PN' + page);
													console.log('Jump to page number: ' + (page+1))
													loopPages(i, page);
												}, 40000);
											}
										}, (page + 1) * 3000);
									}
									
									function catchLog(){
									  	var succeed = document.getElementById("ContentPlaceHolder1_lblSuccess").innerHTML;
										var failed = document.getElementById("ContentPlaceHolder1_lblFailed").innerHTML;
										console.log(succeed);
									  	console.log(failed);

										// totalSucceed = succeed.split(" ")[0];
										// totalFailed = failed.split(" ")[0];
										// succeedItems = [];
										// failedItems = [];

										// if(totalSucceed != 0){
										// 	succeedItems = succeed.split(";")[3];
										// }
										// if(totalFailed != 0){	
										// 	failedItems = failed.split(";")[3];
										// }
										// console.log("finish catchLog() and start nextPage()");
										// nextPage();
									  }
									loopPages(count, page);
								}, products, count)
							)
							.then(function(){

							}, function(err) {
								console.error(err); 	
							});
					    }
				    });
				}else{
					console.log("Failed to load http://192.168.3.52/eMail/Pages/SIAR/LaporanAkun.aspx");
					console.log(error);
				}
			});
		// }, 300000);
	}
};

