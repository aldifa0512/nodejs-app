var mongoose = require('mongoose');

// Role Schema for Role
var RoleSchema = mongoose.Schema({
	role_name 		: { type: String, index:true },
	role_code 		: { type: String },
	description 	: { type: String },
	status			: { type: String }
});

var Role = module.exports = mongoose.model('Role', RoleSchema);

// Create Role Model
module.exports.createRole = function(newRole, callback){
    newRole.save(callback);
}

// Update Role Model
module.exports.updateRole = function(id, data_role, callback){
	Role.findById(id, function (err, role) {
		if (err) return handleError(err);

	    role.set(data_role);
		role.save(callback);
	});
}

// Delete Role Model
module.exports.deleteRole = function(id, callback){
	Role.findByIdAndRemove(id, callback);
}