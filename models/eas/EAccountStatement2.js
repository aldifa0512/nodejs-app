module.exports = {
	run: function() {
		require('events').EventEmitter.prototype._maxListeners = 100;

		var Nightmare = require('nightmare');
		require('nightmare-evaluate-with-callback')(Nightmare);
		var Promise = require('q').Promise;
		var cheerio = require('cheerio');
		var mongoose = require('mongoose');
		var request = require('request');
		var fs = require('fs');

		// var BotSchema = mongoose.Schema({
		// 	tanggal			: {type: String},
		// 	nama_transaksi	: {type: String},
		// 	debit			: {type: String},
		// 	credit			: {type: String}
		// });
		// var Bot = mongoose.model('Bot', BotSchema);

		Nightmare.action('waitforunload', function (done) {
		    this.evaluate_now(function() {
		        return new Promise(function(resolve, reject) {
		            window.onbeforeunload = function() {
		                resolve();
		            };
		        });
		    }, done)
		});

		var nightmare = Nightmare({ show: true });

		function scrapeAccountStatementPage(body, callback){
			var $ = cheerio.load(body);
			var product = {};
			var agent = {};

			// Scrape Product List
			var e = 0;
			$('#ContentPlaceHolder1_cbxProduct_DDD_L_D .dxeListBoxItemRow .dxeListBoxItem').each(function(i){
				var productValue = $(this).html().split(" ")[0];
				productValue = productValue.substr(1).slice(0, -1);
				product[e] = {key: productValue, name: $(this).html()};
				e++;
			});

			// Scrape Agent List
			var f = 0;
			$('#ContentPlaceHolder1_cbxAgent_DDD_L_D .dxeListBoxItemRow .dxeListBoxItem').each(function(i){
				agent[f] = {name: $(this).html()};
				f++;
			});

			fs.writeFile('ftp/accountStatement/product.json', JSON.stringify(product, null, 4), function(err){
			 console.log('Written!, ftp/accountStatement/product.json file');
			})
			fs.writeFile('ftp/accountStatement/agent.json', JSON.stringify(agent, null, 4), function(err){
			 console.log('Written!, ftp/accountStatement/agent.json file');
			})

			callback();
		}

		function startLoopEmail(){
			console.log('Masuk Sini');
			fs.readFile('ftp/accountStatement/product.json', function(err, data) {
			    if(err){
			    	console.log("Failed Reading ftp/accountStatement/product.json");
			    }else{
			    	var date = new Date('2019/03/29').getTime();
			    	var count = 0;
			    	var products = JSON.parse(data);
			    	var totalProducts = Object.keys(products).length;
					var loopProducts = function(p) {
						count++;
					    var product = p.key;
					    var productName = p.name;
					    console.log("Sending E-mail for Product " + productName + " " + " from " + totalProducts);

						Promise.resolve(nightmare
							.goto('http://192.168.3.52/eMail/Pages/SIAR/LaporanAkun.aspx?MenuId=862&ssoid=QYVIBXXOEFO7T0I94VUG73FCZIMXCUOY7SMCVG_5ZA7YF')
							.wait('#ContentPlaceHolder1_cbxProduct_VI')
							.evaluate(function(product) {
								document.querySelector("#ContentPlaceHolder1_cbxProduct_VI").value=product;
							}, product)
							.wait('#ContentPlaceHolder1_dteDate_Raw')
							.evaluate(function(date) {
								document.querySelector("#ContentPlaceHolder1_dteDate_Raw").value=date;
							}, date)
							.wait('#ContentPlaceHolder1_cbxAgent_VI')
							.evaluate(function() {
								document.querySelector("#ContentPlaceHolder1_cbxAgent_VI").value="%";
							})
							.wait('#ContentPlaceHolder1_btnFind_CD')
							.evaluate(function() {
								document.querySelector("#ContentPlaceHolder1_btnFind_CD").click()
							})
							.wait(".dxpSummary")
							.evaluateWithCallback(function (products, i) {
								// Get summary result
								var pageSummary = document.getElementsByClassName("dxpSummary")[0].innerHTML.split(" ");
							   	var totalPages = parseInt(pageSummary[3]);
								var totalResults = parseInt(pageSummary[4].substr(1));
								var count = 0;
								var page = 0;
								var counter = 1;
								var nextProduct = false;
								var timeout = totalPages * 11500;
								function loopPages(count, page){
									setTimeout(function(){
										var loopThisPageNo = (count + 10);
										for (i = count; i < loopThisPageNo; i++) { 
											document.getElementById("ContentPlaceHolder1_ASPxGridView1_DXSelBtn" + i + "_D").className="dxICheckBox  dxWeb_edtCheckBoxChecked";
											console.log("Checking page: " + (page + 1) + " row: " + (i + 1));
										}

										if(totalPages > page){
											page++;
											setTimeout(function(){
												aspxGVPagerOnClick('ContentPlaceHolder1_ASPxGridView1','PN' + page);
												console.log('Jump to page number: ' + (page+1))
												loopPages(i, page);								
											}, 500);
										}
									}, (page + 1) * 3000);
								}
								loopPages(count, page);
								setTimeout(function() {
									// 
								}, timeout);
							}, products, count)
						)
						.then(function(){

						}, function(err) {
							console.error(err); 	
						});
					}
					loopProducts(products[count]);
			    }
		    });
		}

		var SEASM = 'http://192.168.3.52/eMail/Pages/SIAR/LaporanAkun.aspx?MenuId=862&ssoid=QYVIBXXOEFO7T0I94VUG73FCZIMXCUOY7SMCVG_5ZA7YF';
		request(SEASM, function(error, response, body){
			if(!error){
				//scrapeAccountStatementPage(body);
				//startLoopEmail()

				scrapeAccountStatementPage(body, function(){
					startLoopEmail()
				});


			}else{
				console.log("Failed to load http://192.168.3.52/eMail/Pages/SIAR/LaporanAkun.aspx");
				console.log(error);
			}
		});
	}
};

