module.exports = {
	run: function() {
		function parseToUnixTimestamp(s) {
		  var months = {jan:0,feb:1,mar:2,apr:3,may:4,jun:5,
		                jul:6,aug:7,sep:8,oct:9,nov:10,dec:11};
		  var p = s.split('-');
		  var date =  new Date(p[2], months[p[1].toLowerCase()], p[0]);
		  // return Math.floor(date.getTime() / 1000);
		  return Math.floor(date.getTime());
		}

		require('events').EventEmitter.prototype._maxListeners = 100;
		var Promise = require('q').Promise;
		var Nightmare = require('nightmare');
		var cheerio = require('cheerio');
		var mongoose = require('mongoose');
		var request = require('request');
		var fs = require('fs');

		var IMS = 'http://192.168.3.52/siar/';
		var auth = {username: 'P025923', password : 'Bni@123/'};
		var x = Date.now();
		var nightmare = Nightmare({ show: true });

	  nightmare
		.goto(IMS)
		.wait(2000)
		.wait('input[type="Submit"]')
		.type('input[id="txtUserId"]', auth.username)
		.type('input[id="txtPassword"]', auth.password)
		.click('input[type="Submit"]')
		.wait(1000)
		.then(function(){
			request('http://192.168.3.52/eMail/Pages/SIAR/LaporanAkun.aspx?MenuId=862&ssoid=2XHN0HXER2VSWEUT9BU9YQ0BJNSBVCD5BP7I2MM64VZ8F', function(error, response, body){
				if(!error){
				var $ = cheerio.load(body);
				var parameterInput = {};
				var product = {};
				var agent = {};
				var date = '26-Mar-2018';
				// Scrape input and value
				var i = 0;
				$('input').each(function(i)
				{
				 if($(this).attr('name') != null)
					{
						parameterInput[i] = {name: $(this).attr('name'), value: $(this).val()};
					}
					i++;
				});

				// Scrape Product List
				var e = 0;
				$('#ContentPlaceHolder1_cbxProduct_DDD_L_D .dxeListBoxItemRow .dxeListBoxItem').each(function(i)
				{
					product[e] = {name: $(this).html()};
					e++;
				});

				// Scrape Agent List
				var f = 0;
				$('#ContentPlaceHolder1_cbxAgent_DDD_L_D .dxeListBoxItemRow .dxeListBoxItem').each(function(i)
				{
					agent[f] = {name: $(this).html()};
					f++;
				});
				
				fs.writeFile('ftp/accountStatementInputbody.html', JSON.stringify(parameterInput, null, 4), function(err){
				 console.log('File successfully written! - Check your project directory for the ftp/accountStatementInputbody.html file');
				})
				fs.writeFile('ftp/accountStatementProduct.json', JSON.stringify(product, null, 4), function(err){
				 console.log('File successfully written! - Check your project directory for the ftp/accountStatementProduct.json file');
				})
				fs.writeFile('ftp/accountStatementAgent.json', JSON.stringify(agent, null, 4), function(err){
				 console.log('File successfully written! - Check your project directory for the ftp/accountStatementAgent.json file');
				})
				var postUrl = $("form").attr("action");
				
				// Iterate Inputan Json
				var stringFormInput = '';
				for (var i in parameterInput)
				{
					if(parameterInput[i].name == 'ctl00$ContentPlaceHolder1$btnSendEmail')
					{
						continue;
					}
					if(parameterInput[i].name == 'ctl00$ContentPlaceHolder1$cbxProduct')
					{
					 	stringFormInput += ', "' + parameterInput[i].name + '":' + '"' + '[0083154] REKSA DANA SUCORINVEST EQUITY FUND' + '"';
					}
					else if(parameterInput[i].name == 'ctl00$ContentPlaceHolder1$dteDate')
					{
						stringFormInput += ', "' + parameterInput[i].name + '":' + '"' + date + '"';
					}
					else if(parameterInput[i].name == 'ContentPlaceHolder1_cbxProduct_VI')
					{
						stringFormInput += ', "' + parameterInput[i].name + '":' + '"' + '0083154' + '"';
					}
					else if(parameterInput[i].name == 'ctl00$ContentPlaceHolder1$cbxAgent')
					{
						stringFormInput += ', "' + parameterInput[i].name + '":' + '"' + 'All' + '"';
					}
					else if(parameterInput[i].name == 'ctl00$ContentPlaceHolder1$dteDate$DDD$C')
					{
						stringFormInput += ', "' + parameterInput[i].name + '":' + '"' + '03/27/2018:03/26/2018' + '"';
					}
					else if(parameterInput[i].name == 'ContentPlaceHolder1_cbxAgent_VI')
					{
						stringFormInput += ', "' + parameterInput[i].name + '":' + '"' + '%' + '"';
					}
					else if(parameterInput[i].name == 'ctl00$ContentPlaceHolder1$cbxAgent$DDD$L')
					{
						stringFormInput += ', "' + parameterInput[i].name + '":' + '"' + '%' + '"';
					}
					else if(parameterInput[i].name == 'ctl00$ContentPlaceHolder1$ASPxGridView1$DXSelInput')
					{
						stringFormInput += ', "' + parameterInput[i].name + '":' + '"' + '' + '"';
					}
					else if(parameterInput[i].name == 'ctl00$ContentPlaceHolder1$cbx_extention_format$DDD$L')
					{
						stringFormInput += ', "' + parameterInput[i].name + '":' + '"' + 'pdf' + '"';
					}
					else if(parameterInput[i].name == 'ctl00$ContentPlaceHolder1$txtValue')
					{
						stringFormInput += ', "' + parameterInput[i].name + '":' + '"' + '' + '"';
					}
					else if(parameterInput[i].name == 'ctl00$ContentPlaceHolder1$cbxProduct$DDD$L')
					{
						stringFormInput += ', "' + parameterInput[i].name + '":' + '"' + '0083154' + '"';
					}
					else if (parameterInput[i].name == 'ContentPlaceHolder1_dteDate_Raw')
					{
						stringFormInput += ', "' + parameterInput[i].name + '":' + '"' + parseToUnixTimestamp(date) + '"';	
					}
					else
					{
						stringFormInput += ', "' + parameterInput[i].name + '":' + '"' + parameterInput[i].value + '"';
					}
				}
				stringFormInput += ', "' + 'DXScript' + '":' + '"' + '1_44,1_76,1_59,2_34,2_41,1_48,1_56,2_33,1_69,1_67,2_36,2_28,2_27,1_54,3_7,2_30' + '"';
				
				var newstr = stringFormInput.substr(1);
				newstr = '[{' + newstr + '}]';

				var jsonStrin = JSON.parse(newstr);

				fs.writeFile('ftp/accountStatementInput.json', JSON.stringify(jsonStrin[0], null, 4), function(err){
					console.log('File successfully written! - Check your project directory for the ftp/accountStatementInput.json file');
				})

				request.post(
				{
					url:'http://192.168.3.52/eMail/Pages/SIAR/LaporanAkun.aspx?MenuId=862&ssoid=_QBNYEMXP3TSMFC34DQUXR9CPK0P0V5QTB2MD_97O6Z7M',
					form: jsonStrin[0]
		}, function(err,httpResponse,body)
		{
			if(err) console.log(err);

			var $ = cheerio.load(body);
			
			var parameterInput = '';
			$('#ContentPlaceHolder1_ASPxGridView1 .dxgvDataRow input').each(function(i)
			{
				console.log($(this).attr("name"));
				if($(this).attr('name') != null)
				{
					parameterInput[i] = {name: $(this).attr('name'), value: $(this).val()};
					console.log(parameterInput[i]);
				}
				i++;
			});

			fs.writeFile('ftp/accountStatementInput2.json', JSON.stringify(parameterInput, null, 4), function(err){
				console.log('File successfully written! - Check your project directory for the ftp/accountStatementInput2.json file');
			})
			
			fs.writeFile('ftp/accountStatementResposenBody.html', body, function(err){
				console.log('File successfully written! - Check your project directory for the ftp/accountStatementResposenBody.html file');
			})
		});
	}
});
})
.catch(function (error) {
console.error('Error:', error);
});
}
}