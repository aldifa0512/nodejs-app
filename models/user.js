var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
const saltRounds = 10;

// User Schema
var UserSchema = mongoose.Schema({
	username 			: { type: String,index:true },
	password 			: { type: String },
	email 				: { type: String },
	fullname			: { type: String },
	telephone			: { type: String },
	access_level		: { type: String },
	language			: { type: String },
	user_POS			: { type: String },
	printing_profile	: { type: String },
	use_popup			: { type: String }
});

var User = module.exports = mongoose.model('User', UserSchema);

module.exports.createUser = function(newUser, callback){

	bcrypt.genSalt(saltRounds, function(err, salt) {
	    bcrypt.hash(newUser.password, salt, function(err, hash) {
	        newUser.password = hash;
        	newUser.save(callback);
	    });
	});
}

module.exports.updateUser = function(id, data_user, callback){
	User.findById(id, function (err, user) {
		if (err) return handleError(err);

		bcrypt.genSalt(saltRounds, function(err, salt) {
		    bcrypt.hash(newUser.password, salt, function(err, hash) {
		        data_user.password = hash;
		        console.log(data_user.password);
			    user.set(data_user);
				user.save(callback);
		    });
		});
	});
}

module.exports.deleteUser = function(id, callback){
	User.findByIdAndRemove(id, callback);
}

module.exports.getUserByUsername = function(username, callback){
	var query = {username: username};
	User.findOne(query, callback);
}

module.exports.getUserById = function(id, callback){
	User.findById(id, callback);
}

module.exports.comparePassword = function(candidatePassword, hash, callback){
	console.log('Candidate Password : ' + candidatePassword);
	
	bcrypt.compare(candidatePassword, hash, function(err, res) {
	    if (err) { throw (err); }
        console.log("isMatched: " + res);
	 	callback(null, res);
	});
}