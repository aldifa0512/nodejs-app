var sync = require('synchronize')
var fs   = require('fs')
var Nightmare = require('nightmare');
require('nightmare-evaluate-with-callback')(Nightmare);
require('events').EventEmitter.prototype._maxListeners = 100;

// sync(fs, 'readFile')

// sync.fiber(function(){
//   var data = fs.readFile(__filename, 'utf8')
//   console.log(data)

//   try {
//     data = fs.readFile('invalid', 'utf8')
//   } catch (err) {
//     console.log(err)
//   }

//   fs.readFile(__filename, 'utf8', function(err, data){
//     console.log(data)
//   })
// })

Nightmare.action('waitforunload', function (done) {
    this.evaluate_now(function() {
        return new Promise(function(resolve, reject) {
            window.onbeforeunload = function() {
                resolve();
            };
        });
    }, done)
});