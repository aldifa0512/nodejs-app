var mongoose = require('mongoose');

// Menu Schema for Menu
var MenuSchema = mongoose.Schema({
	menu_name 		: { type: String, index:true },
	menu_role 		: { type: String },
	description 	: { type: String },
	status			: { type: String }
});

var Menu = module.exports = mongoose.model('Menu', MenuSchema);

// Get all Menu 
module.exports.getAllMenu = function(callback){
	Menu.find({}, 'menu_name', function(err, menus){
        if(err){
          console.log(err);
        } else{
        	callback;
        }
    });
}

// Create Menu Model
module.exports.createMenu = function(newMenu, callback){
    newMenu.save(callback);
}

// Update Menu Model
module.exports.updateMenu = function(id, data_menu, callback){
	Menu.findById(id, function (err, role) {
		if (err) return handleError(err);

	    role.set(data_menu);
		role.save(callback);
	});
}

// Delete Menu Model
module.exports.deleteMenu = function(id, callback){
	Menu.findByIdAndRemove(id, callback);
}